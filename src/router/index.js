import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeContent from '../components/HomeContent.vue'

Vue.use(VueRouter)

const routes = [
   {
        path: '/',
        name: 'Home',
        component: HomeContent
    },
    {
        path: '/teams',
        name: 'Teams',
        component: () => import('../components/TeamContent.vue')
    },
    {
        path: '/teams/:teamsId',
        name: 'TeamsDetails',
        component: () => import('../components/TeamsDetails.vue')
    },


]

const router = new VueRouter({
  routes
})

export default router
